# Ejercicio 2. Maquetado CSS.
Este repositorio corresponde al video de prácticas de CSS planteado en: 
- [Cómo escribir CSS | Clase 6](https://youtu.be/50xmoVtuC_4 "Youtube")
- [Pasando del diseño a CSS | Clase 7](https://youtu.be/rUHAYy_ZrKA "Youtube")

## Consigna.
---
En base a la estructura HTML trabajada en [Explicación del ejercicio de HTML | Clase 5](https://youtu.be/fyHhEoonh74 "Youtube") deberás aplicar selectores y reglas para que el sitio se vea como el diseño planteado.

### Página Home
---
<img src="./imagenes/home.png" width="960px" />

### Página Contacto
---
<img src="./imagenes/contacto.png" width="960px" />